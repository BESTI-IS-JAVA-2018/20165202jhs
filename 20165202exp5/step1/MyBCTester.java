import java.util.Scanner;
import java.lang.String;

public class MyBCTester  {

	public static void main (String[] args) {

		String expression, again;

		String result_1;
		int result_2;

		try
		{
			Scanner in = new Scanner(System.in);

			do
			{
				MyBC bc = new MyBC();
				System.out.println ("请输入一个中缀表达式： ");
				expression = in.nextLine();

				result_1 = bc.parse(expression);
				System.out.println();

				String[] b = result_1.split("");
				String x = "";
				for(int i=0;i<b.length;i++){
					x=x+b[i]+" ";
				}
				System.out.println("后缀表达式为： " + x);

				MyDC dc = new MyDC();
				result_2 = dc.evaluate (x);
				System.out.println ("That expression equals " + result_2);

				System.out.print ("Evaluate another expression [Y/N]? ");
				again = in.nextLine();
				System.out.println();
			}
			while (again.equalsIgnoreCase("y"));
		}
		catch (Exception IOException)
		{
			System.out.println("Input exception reported");
		}
	}
}
