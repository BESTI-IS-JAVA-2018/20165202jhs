public class Test {
	public static void main(String[] args) {
		CPU cpu0 = new CPU();
		CPU cpu1 = new CPU();
		cpu0.setSpeed(2200);
		cpu1.setSpeed(1500);
		System.out.println("cpu0:"+cpu0.toString());
		System.out.println("cpu1:"+cpu1.toString());
		System.out.println("cpu0==cpu1:"+cpu0.equals(cpu1));
		HardDisk disk0 = new HardDisk();
		HardDisk disk1 = new HardDisk();
		disk0.setAmount(200);
		disk1.setAmount(100);
		System.out.println("disk0:"+disk0.toString());
		System.out.println("disk1:"+disk1.toString());
		System.out.println("disk0==disk1:"+disk0.equals(disk1));
		PC pc0 = new PC();
		PC pc1 = new PC();
		pc0.setCpu(cpu0);
		pc0.setHardDisk(disk0);
		pc1.setCpu(cpu0);
		pc1.setHardDisk(disk0);
		pc0.show();
		pc1.show();
		System.out.println("pc0:"+pc0.toString());
		System.out.println("pc1:"+pc1.toString());
		System.out.println("pc0==pc1:"+pc0.equals(pc1));
	}
}
