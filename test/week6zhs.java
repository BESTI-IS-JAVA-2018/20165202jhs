public class week6zhs {
	public static void main(String args[]) {
		int c=Integer.parseInt(args[0]);
		int d=Integer.parseInt(args[1]);
		int result=judge(c,d);
		if(result==0) {
			System.out.println("非法输入！");
		}
		else {
			System.out.println(result);
		}
	}
	public static int judge(int c,int d) {
		if(c<d||c<0||d<0){
			return 0;
		}
		else if(d==c||d==0) {
			return 1;
		}
		else {
			return judge(c-1,d-1)+judge(c-1,d);
		}
	}
}

