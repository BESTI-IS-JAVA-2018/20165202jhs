import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
	Complex p1=new Complex(1,1);
	Complex p2=new Complex(1,-1);
	@Test
		public void testAdd(){
			assertEquals("2.0+0.0i",p1.ComplexAdd(p2).toString());
			System.out.println(p1.ComplexAdd(p2));
		}
	@Test
		public void testSub(){
			assertEquals("0.0+2.0i",p1.ComplexSub(p2).toString());
			System.out.println(p1.ComplexSub(p2));
		}
	public void testMuti(){
		assertEquals("2.0+0.0i",p1.ComplexMulti(p2).toString());
		System.out.println(p1.ComplexSub(p2));
	}
	public void testDiv(){
		assertEquals("0.0+1.0i",p1.ComplexDiv(p2).toString());
		System.out.println(p1.ComplexDiv(p2));
	}
}
