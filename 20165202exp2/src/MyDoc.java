abstract class Data{
	public abstract void DisplayValue();
}
class Integer extends Data {
	int value;
	Integer(){
		value=100;
	}
	public void DisplayValue(){
		System.out.println(value);
	}
}
class Boolean extends Data{
	boolean value;
	Boolean(){
		value=true;
	}
	public void DisplayValue(){
		System.out.println(value);
	}
}
class Document {
	Data pd;
	Document() {
		pd=new Boolean();
	}
	public void DisplayData(){
		pd.DisplayValue();
	}
}
public class MyDoc {
	static Document d;
	public static void main(String[] args) {
		d = new Document();
		d.DisplayData();
	}
}
