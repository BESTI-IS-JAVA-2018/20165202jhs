import static org.junit.Assert.*;
import org.junit.Test;
import junit.framework.TestCase;
public class ComplexTest extends TestCase {
	Complex complex = new Complex(1,1);
	@Test
		public void testAdd(){
			assertEquals(new Complex(6.6,3.8), complex.ComplexAdd(new Complex(5.6,2.8)));
		}
	//测试加法
	@Test
		public void testSub(){
			assertEquals(new Complex(-5.8,-3.3), complex.ComplexSub(new Complex(6.8,4.3)));
		}
	//测试减法
	@Test
		public void testMulti(){
			assertEquals(new Complex(6.6,6.9), complex.ComplexMulti(new Complex(6.6,6.9)));
		}
	//测试乘法
	@Test
		public void testDiv(){
			assertEquals(new Complex(2.0,2.0), complex.ComplexDiv(new Complex(2.0,2.0)));
			assertEquals(new Complex(0.0,0.0), complex.ComplexDiv(new Complex(1.0,0.0)));
			//assertEquals(new Complex(0.0,0.0), complex.ComplexDiv(new Complex(3,4)));
			//边缘测试
		}
	@Test
		public void testequals(){
			assertEquals(true, complex.equals(new Complex(1.0,1.0)));
		}
	//测试判断相等
}
