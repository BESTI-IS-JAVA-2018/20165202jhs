import java.lang.Integer;
import java.util.Objects;

public class Complex {
	//定义属性并生成getter,setter
	double RealPart;
	double ImagePart;
	public double getRealPart(){
		return RealPart;
	}
	public double getImagePart(){
		return ImagePart;
	}

	//定义构造函数
	public Complex(){
		RealPart = 0;
		ImagePart = 1;
	}
	public Complex(double R,double I){
		RealPart = R;
		ImagePart = I;
	}

	//Override Object
	@Override
		public boolean equals(Object obj){
			if(this == obj){
				return true;
			}
			if(!(obj instanceof Complex)) {
				return false;
			}
			Complex complex = (Complex) obj;
			if(complex.RealPart != ((Complex) obj).RealPart) {
				return false;
			}
			if(complex.ImagePart != ((Complex) obj).ImagePart) {
				return false;
			}
			return true;
		}
	@Override
		public String toString(){
			String s = new String();
			if (ImagePart > 0){
				s = getRealPart() + "+" + getImagePart() + "i";
			}
			if (ImagePart == 0){
				s = getRealPart() + "";
			}
			if(ImagePart < 0){
				s = getRealPart() + "" + getImagePart() + "i";
			}
			if(RealPart == 0){
				s = getImagePart() + "i";
			}
			return s;
		}
	//定义公有方法：加减乘除
	Complex ComplexAdd(Complex a){
		return new Complex(RealPart + a.RealPart,ImagePart + a.ImagePart);
	}
	Complex ComplexSub(Complex a){
		return new Complex(RealPart - a.RealPart,ImagePart - a.ImagePart);
	}
	Complex ComplexMulti(Complex a){
		return new Complex(RealPart*a.RealPart-ImagePart*a.ImagePart,RealPart*a.ImagePart + ImagePart*a.RealPart);
	}
	Complex ComplexDiv(Complex a) {
		return new Complex((RealPart * a.ImagePart + ImagePart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart), (ImagePart * a.ImagePart + RealPart * a.RealPart) / (a.RealPart * a.RealPart + a.RealPart * a.RealPart));
	}
}
