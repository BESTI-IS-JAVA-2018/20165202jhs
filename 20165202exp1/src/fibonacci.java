import java.util.Scanner;  

public class fibonacci {  

	public static void main(String[] args) {  

		Scanner scanner = new Scanner(System.in);  

		System.out.println("请输入一个整数：");  

		int n = scanner.nextInt();  

		System.out.println("fibonacci数列为：");  
		if(n<=0) {
			System.out.println("非法输入");
		}
		else { 
		  for (int j = 1; j <= n; j++) {  

			System.out.println(" a["+j+"] = "+fibonacci(j));  

		  }  
		}
		scanner.close();  

	}  

	private static int fibonacci(int i) {  

		if (i == 1 || i == 2){  

			return 1;  

		}else{  

			return fibonacci(i - 1) + fibonacci(i - 2);  

		}  

	}  

}  

