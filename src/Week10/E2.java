import java.util.*;
class Student implements Comparable {
	int english=0;
	String name;
	Student(int english,String name) {
		this.name=name;
		this.english=english;
	}
	public int compareTo(Object b) {
		Student st=(Student)b;
		return (this.english-st.english);
	}
}
public class E2 {
	public static void main(String args[]) {
		List<Student> list=new LinkedList<Student>();
		int score []={100,76,45,99,77,88,75,79};
		String name[]={"一文","李悦","江流","胡克","魏凡","周平","赵剑","魏派"};
		for(int i=0;i<score.length;i++){
			list.add(new Student(score[i],name[i]));
		}
		Iterator<Student> iter=list.iterator();
		TreeSet<Student> mytree=new TreeSet<Student>();
		while(iter.hasNext()){
			Student stu=iter.next();
			mytree.add(stu);
		}
		Iterator<Student> te=mytree.iterator();
		while(te.hasNext()) {
			Student stu=te.next();
			System.out.println(""+stu.name+" "+stu.english);
		}
	}
}
