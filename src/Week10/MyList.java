import java.util.*;
class Stu implements Comparable{
	int id;
	String name;
	Stu(String n, int i){
		name=n;
		id=i;
	}
	public int compareTo(Object b){
		Stu st=(Stu)b;
		return (this.id-st.id);
	}
}
public class MyList {
	public static void main(String [] args) {
		//选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
		LinkedList<Stu> list=new LinkedList<>();
		list.add(new Stu("其米仁增",5239));
		list.add(new Stu("李梓豪",5201));
		list.add(new Stu("夏云霄",5203));
		list.add(new Stu("贾普涵",5204));
		//把上面四个节点连成一个没有头结点的单链表
		Iterator<Stu> iter=list.iterator();
		//遍历单链表，打印每个结点的
		System.out.println("初始单链表为：");
		while (iter.hasNext()){
			Stu st=iter.next();
			System.out.println(st.id+" "+st.name);
		}
		//把你自己插入到合适的位置（学号升序）
		list.add(new Stu("贾海粟",5202));
		Collections.sort(list);
		//遍历单链表，打印每个结点的
		iter=list.iterator();
		System.out.println("插入我的学号和姓名后单链表为：");
		while (iter.hasNext()){
			Stu st=iter.next();
			System.out.println(st.id+" "+st.name);
		}
		//从链表中删除自己
		list.remove(1);
		iter=list.iterator();
		//遍历单链表，打印每个结点的
		System.out.println("删除我的学号和姓名后单链表为：");
		while (iter.hasNext()){
			Stu st=iter.next();
			System.out.println(st.id+" "+st.name);
		}

	}
}
