
package sizeyunsuan;
import java.util.Stack;
import java.util.StringTokenizer;
public class Jisuan {
	private final char ADD = '+';
	private final char SUBTRACT = '-';
	private final char MULTIPLY = '*';
	private final char DIVIDE = '/';
	private Stack<String> stack;
	private Shu s1,s2;
	public Jisuan() {
		stack = new Stack<String>();
	}
	//栈
	public String evaluate (String expr) {
		Shu computingresult = null;
		String op1, op2,result = "";
		int numer1, denom1,numer2,denom2;
		String token;
		StringTokenizer tokenizer = new StringTokenizer(expr);
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
			if (isOperator(token)) {
				op1 = stack.pop();
				op2 = stack.pop();
				StringTokenizer tokenizer1 = new StringTokenizer(op2, "/");
				numer1 = Integer.parseInt(tokenizer1.nextToken());
				if (tokenizer1.hasMoreTokens())
					denom1 = Integer.parseInt(tokenizer1.nextToken());
				else denom1 = 1;
				s1 = new Shu(numer1,denom1);
				StringTokenizer tokenizer2 = new StringTokenizer(op1, "/");
				numer2 = Integer.parseInt(tokenizer2.nextToken());
				if (tokenizer2.hasMoreTokens())
					denom2 = Integer.parseInt(tokenizer2.nextToken());
				else denom2 = 1;
				s2 = new Shu(numer2,denom2);
				computingresult = CalculateOp(token.charAt(0),s1,s2);
				stack.push(computingresult.toString());
			}else
				stack.push(token);
		}
		result = stack.pop();
		return result;
	}
	//符号匹配
	private boolean isOperator (String token){
		return ( token.equals("+") || token.equals("-") ||
				token.equals("*") || token.equals("/") );
	}
	//符号计算
	private Shu CalculateOp(char operation, Shu op1, Shu op2)
	{
		Shu result = null;
		switch (operation)
		{
			case ADD:
				result = op1.add(op2);
				break;
			case SUBTRACT:
				result = op1.subtract(op2);
				break;
			case MULTIPLY:
				result = op1.multiply(op2);
				break;
			case DIVIDE:
				result = op1.divide(op2);
				break;
		}
		return result;
	}
}
