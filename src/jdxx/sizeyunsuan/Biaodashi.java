
package sizeyunsuan;
import java.util.*;
import java.util.Random;
import java.io.IOException;
import java.util.ArrayList;
import java.text.NumberFormat;
public class Biaodashi {
	protected NumberFormat nf = NumberFormat.getPercentInstance();
	Random random = new Random();
	private String Oper = "";
	protected int ExpreNum = 0;
	protected int trues;;
	protected List<String> list;
	private List<String> list2;
	protected List<String> list3;
	public Biaodashi() {
		list = new ArrayList<String>();
		list2 = new ArrayList<String>();
		list3 = new ArrayList<String>();
	}
	public void CreateExpre(int number, int many, String language) {
		if(language.equalsIgnoreCase("A"))
			many = ClassC(many);
		else if(language.equalsIgnoreCase("B"))
			many = ClassE(many);
		else many = ClassF(many);
		for (int j = 0; j < number; j++) {
			String ti = "";
			for (int i = 0; i < many; i++) {
				int A = random.nextInt(20) + 1;
				int D = random.nextInt(20) + 1;
				int B = random.nextInt(5);
				int C = random.nextInt(5);
				Shu si = new Shu(A, D);
				if (parity(i)) {
					if (list2.indexOf("( ") == -1)
						list2.add(getOper() + " ");
					else if (list2.size() - list2.lastIndexOf("( ") > 4) {
						if (list2.lastIndexOf(") ") - list2.lastIndexOf("( ") < 0 && B == 0) {
							list2.add(") ");
							list2.add(getOper() + " ");
						} else list2.add(getOper() + " ");
					} else list2.add(getOper() + " ");
				} else if (i == many - 1) {
					if (list2.lastIndexOf("( ") - list2.lastIndexOf(") ") > 0) {
						if (C == 0) {
							list2.add(si.toString() + " ");
							list2.add(") ");
						} else {
							list2.add(A + " ");
							list2.add(") ");
						}
					} else if (C != 0)
						list2.add(A + " ");
					else list2.add(si.toString() + " ");
				} else if (i == 0) {
					if (C != 0)
						list2.add(A + " ");
					else list2.add(si.toString() + " ");
				} else if (list2.lastIndexOf(") ") != -1) {
					if (list2.lastIndexOf(") ") - list2.lastIndexOf("( ") > 0 && B == 0) {
						list2.add("( ");
						if (C != 0)
							list2.add(A + " ");
						else list2.add(si.toString() + " ");
					} else if (C != 0)
						list2.add(A + " ");
					else list2.add(si.toString() + " ");
				} else if (list2.indexOf("( ") == -1 && B == 0) {
					list2.add("( ");
					if (C != 0)
						list2.add(A + " ");
					else list2.add(si.toString() + " ");
				} else if (C != 0)
					list2.add(A + " ");
				else list2.add(si.toString() + " ");
			}
			for (String i : list2)
				ti += i;
			list2.clear();
			list.add(ti);
		}
	}
	public void showC() {
		trues = 0;
		Fuhao infixTosuffix = new Fuhao();
		Jisuan cal = new Jisuan();
		Scanner scan = new Scanner(System.in);
		ListIterator<String> li = list.listIterator();
		while (li.hasNext()) {
			String A = li.next();
			li.remove();
			infixTosuffix.evaluate(A);
			String result = cal.evaluate(infixTosuffix.getMessage());

			System.out.print("\n" + A + " = ");
			String B = scan.next();
			if (result.equals(B)) {
				System.out.println("正确");
				trues++;
			} else {
				System.out.println("错误正确答案为" + result);
			}
		}
	}
	public void showE() {
		trues = 0;
		Fuhao infixTosuffix = new Fuhao();
		Jisuan cal = new Jisuan();
		Scanner scan = new Scanner(System.in);
		ListIterator<String> li = list.listIterator();
		while (li.hasNext()) {
			String A = li.next();
			li.remove();
			infixTosuffix.evaluate(A);
			String result = cal.evaluate(infixTosuffix.getMessage());

			System.out.print("\n" + A + " = ");
			String B = scan.next();
			if (result.equals(B)) {
				System.out.println("Congratulations,you got it!");
				trues++;
			} else {
				System.out.println("Sorry,the right answer is " + result);
			}
		}
	}
	public void showF() {
		trues = 0;
		Fuhao infixTosuffix = new Fuhao();
		Jisuan coun = new Jisuan();
		Scanner scan = new Scanner(System.in);
		ListIterator<String> li = list.listIterator();
		while (li.hasNext()) {
			String A = li.next();
			li.remove();
			infixTosuffix.evaluate(A);
			String result = coun.evaluate(infixTosuffix.getMessage());

			System.out.print("\n" + A + " = ");
			String B = scan.next();
			if (result.equals(B)) {
				System.out.println("正確");
				trues++;
			} else {
				System.out.println("錯誤正確答案為" + result);
			}
		}
	}
	public int getTrues() {
		return trues;
	}
	public String getOper() {
		int A = random.nextInt(4);
		switch (A) {
			case 0:
				Oper = "+";
				break;
			case 1:
				Oper = "-";
				break;
			case 2:
				Oper = "*";
				break;
			case 3:
				Oper = "/";
				break;
		}
		return Oper;
	}
	public boolean parity(int num) {
		if (num % 2 == 1)
			return true;
		else
			return false;
	}
	public int ClassC(int many) {
		Scanner scan = new Scanner(System.in);
		int A = 1;
		while (true) {
			try {
				if (many > 0) {
					for (int i = 0; i < many; i++) {
						A += 2;
					}
					break;
				} else throw new Exception();
			} catch (Exception e) {
				System.out.println("级别输入错误请重新输入要求级别至少为1");
				many = scan.nextInt();
			}
		}
		return A;
	}
	public int ClassE(int many) {
		Scanner scan = new Scanner(System.in);
		int A = 1;
		while (true) {
			try {
				if (many > 0) {
					for (int i = 0; i < many; i++) {
						A += 2;
					}
					break;
				} else throw new Exception();
			}
			catch (Exception e) {
				System.out.println("The level of questions is incorrect, Please re-enter it (at least 1)");
				many = scan.nextInt();
			}
		}
		return A;
	}
	public int ClassF(int many) {
		Scanner scan = new Scanner(System.in);
		int A = 1;
		while (true) {
			try {
				if (many > 0) {
					for (int i = 0; i < many; i++) {
						A += 2;
					}
					break;
				} else throw new Exception();
			}
			catch (Exception e) {
				System.out.println("級別輸入錯誤請重新輸入要求級別至少為1");
				many = scan.nextInt();
			}
		}
		return A;
	}
	public void outFile(String s, String s1) throws IOException {
	}
	public void show() {
	}
}
