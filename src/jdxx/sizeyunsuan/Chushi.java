
package sizeyunsuan;
import java.util.Scanner;
public class Chushi {
	public static String language;
	public static void main(String[] args) {
		Jieguo iopra = new Jieguo();
		Scanner scan = new Scanner(System.in);
		System.out.print("选择语言 (Select language)\t\t\tA)简体中文\t\tB)English\t\tC)繁體中文" +
				"\n输入1,2,3 (Input 1,2,3)");
		language = scan.nextLine();
		while (true) {
			if (language.equalsIgnoreCase("1")) {
				System.out.print("输入需要题目的数量");
				int number = scan.nextInt();
				while (true) {
					try {
						if (number > 0) {
							System.out.print("\n提醒初始等级为1即两个数的运算每加一级多一个字符和一个数\n\n请输入等级");
							int many = scan.nextInt();
							iopra.CreateExpre(number, many, language);
							iopra.inFile("src/sizeyunsuan/SIZEYUNSUAN.txt");
							break;
						} else throw new Exception();
					} catch (Exception e) {
						System.out.println("题目数量输入错误请重新输入要求至少为1");
						number = scan.nextInt();
					}
				}
				break;
			} else if (language.equalsIgnoreCase("2")) {
				System.out.print("The number of questions you need: ");
				int number = scan.nextInt();
				while (true)
					try {
						if (number > 0) {
							System.out.print("\ne.g. a + b is a level 1 and when adding a level," +
									"(meanwhile) increasing a digit and an operator" +
									"\nThe level of questions you need:");
							int many = scan.nextInt();
							iopra.CreateExpre(number, many, language);
							iopra.inFile("src/sizeyunsuan/SIZEYUNSUAN.txt");
							break;
						} else throw new Exception();
					} catch (Exception e) {
						System.out.println("The number of questions is incorrect, Please re-enter it (at least 1)");
						number = scan.nextInt();
					}
				break;
			} else if (language.equalsIgnoreCase("3")) {
				System.out.print("輸入需要題目的數量");
				int number = scan.nextInt();
				while (true)
					try {
						if (number > 0) {
							System.out.print("\n提醒初始等級為1即兩個數的運算每加一級多一個字符和一個數\n\n請輸入等級:");
							int many = scan.nextInt();
							iopra.CreateExpre(number, many, language);
							iopra.inFile("src/sizeyunsuan/SIZEYUNSUAN.txt");
							break;
						} else throw new Exception();
					} catch (Exception e) {
						System.out.println("題目數量輸入錯誤請重新輸入要求至少為1");
						number = scan.nextInt();
					}
				break;
			} else
				while (!language.equalsIgnoreCase("1") && !language.equalsIgnoreCase("B") && !language.equalsIgnoreCase("C")) {
					System.out.println("语言选择错误!请重新输入1,2,3");
					language = scan.nextLine();
				}
		}
	}
}
