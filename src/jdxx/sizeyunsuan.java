import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.lang.String;
public class sizeyunsuan {
	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);
		System.out.print("请输入你想要做的题目的数量:");
		int num = scanner.nextInt();
		double result=0;
		double answer,accuracy;
		double right=0;
		for (int i=0;i<num;i++) {
			Random random = new Random();
			int num_1 = random.nextInt(100);
			int num_2 = random.nextInt(100);
			System.out.print("题目 "+(i+1)+" :\n");
			switch (random.nextInt(4)) {
				case 0:
					result=Jisuan.add(num_1,num_2);
					System.out.println(num_1+" + "+num_2);
					break;
				case 1:
					result=Jisuan.sub(num_1,num_2);
					System.out.println(num_1+" - "+num_2);
					break;
				case 2:
					result=Jisuan.mul(num_1,num_2);
					System.out.println(num_1+" * "+num_2);
					break;
				case 3:
					result=Jisuan.div(num_1,num_2);
					System.out.println(num_1+" / "+num_2);
					break;
			}
			System.out.println("请输入你的答案:");
			answer = scanner.nextInt();
			if (answer == result) {
				System.out.println("Right !!！");
				right++;
			} else
				System.out.println("Wrong !!! \nThe correct answer is : " + result);
		}
		accuracy = (double) (right / num) * 100;
		if(num==0) {
			System.out.println("答题结束 ！！！");
		}
		else{
			System.out.println("答题完毕 ！\n 你的正确率为 ：" + accuracy + "%");
		}
	}

}

class Jisuan {
	static double add(int num_1,int num_2) {
		return (num_1+num_2);
	}

	static double sub(int num_1,int num_2) {
		return (num_1-num_2);
	}

	static double mul(int num_1,int num_2) {
		return (num_1*num_2);
	}

	static double div(int num_1,int num_2) {
		double temp;
		if (num_2!=0) {
			temp=num_1/num_2;
		}
		else
			temp=0;
		return temp;
	}
}
