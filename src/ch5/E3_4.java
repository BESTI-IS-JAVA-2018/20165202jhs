class A{
	int m;
	int getM(){
		return m;
	}
	int seeM(){
		return m;
	}
}
class B extends A{
	int m;
	int getM(){
		return m+100;
	}
}
public class E3_4{
	public static void main(String args[]){
		B b = new B();
		b.m = 20;
		System.out.println(b.getM());//【代码1】
		A a = b;
		a.m = -100;        //上转型对象访问的是被隐藏的m
		System.out.println(a.getM());//【代码2】上转型对象调用的一定是子类重写的getM()方法
		System.out.println(b.seeM());//【代码3】子类继承的seeM()方法操作的m是被子类隐藏的m
	}
}
