 # **20165202 2017-2018-2 《Java程序设计》第1周学习总结** #
 ## **教材学习内容总结** ##
 - Ubuntu环境下安装JDK
 - 简单Java程序编写
 - 反编译器javap.exe
 - Git安装及使用方法
 - 学习Vim使用方法

 ## **教材学习中的问题和解决过程** ##

 - Ubuntu内无法安装Java
 解决方法：
 参考《[如何在Ubuntu通过PPA安装JAVA 9](https://www.howtoing.com/install-oracle-java-9-jdk-9-ubuntu-via-ppa/)》
 添加webupd8team的Java 9 PPA库到系统并安装Oracle JAVA 9
 ```$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get install oracle-java9-installer
```

验证Java版本
 ```$ java -version```

 - 码云托管无法获取公钥
 ![输入图片说明](https://gitee.com/uploads/images/2018/0304/183642_4c944763_1745060.jpeg "3.jpg")
 
解决方法：
参考胡东晖学长的 《[使用git推送代码到开源中国以及IDEA环境下使用git](http://www.cnblogs.com/rebrust/p/5348866.html)》
![公钥解决](https://gitee.com/uploads/images/2018/0304/183345_3f55f54b_1745060.png "git的公钥.png")

安装过程中，询问是否修改环境变量，选择第二项“Use Git from the Windows Command Prompt”，可在Windows的命令行cmd中运行git命令（这样很方便）
通过git安装路径找打```git-bash.exe```，双击运行，并在弹出的命令窗口中输入以下指令
 ```ssh-keygen -t rsa -C "youreamil@xxx.com"```

连续输入回车，会在当前用户文件夹下生成一个名为.ssh的文件夹，用记事本打开该文件夹中名为```id_rsa.pub```的文件并复制里面的全部内容.。即可获得公钥

## **代码调试中的问题和解决过程** ##

 - 通过```vi src/Hello.java```编辑代码无法输入
 解决方法：学习vim使用
 i	在当前光标处进行编辑
I	在行首插入
A	在行末插入
a	在光标后插入编辑
o	在当前行后插入一个新行
O	在当前行前插入一个新行
cw	替换从光标所在位置后到一个单词结尾的字符
从普通模式输入:进入命令行模式，输入wq回车，保存并退出编辑

 - ```git add .```命令无效

![仓库](https://gitee.com/uploads/images/2018/0304/183425_3196fd9d_1745060.jpeg "未建立仓库错误.jpg")

 解决方法：
 输入```git init```建立空仓库
 - ```git push```报错：无对应上游分支
 解决方法：
 在正确路径建立空仓库，如```cd 20165202jhs```进入克隆项目所在目录
 - 通过```vim run.sh```写一个脚本简化编译过程无法打开
 解决方法：
 未安装vim 使用 ```sudo apt-get install vim```
 
 - 脚本运行失败

![脚本运行失败](https://gitee.com/uploads/images/2018/0304/183502_c5a00707_1745060.png "6脚本运行.png")

解决方法
提醒了我要注意大小写及中英输入法的问题，“；”和“：”的差别很大

![输入图片说明](https://gitee.com/uploads/images/2018/0304/183523_b7f2653b_1745060.png "6脚本运行.png")

## **代码托管** ##

[码云链接](https://gitee.com/BESTI-IS-JAVA-2018/20165202jhs)

![输入图片说明](https://gitee.com/uploads/images/2018/0304/195710_3432cc3b_1745060.jpeg "zhubajie.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0304/195506_456b8c59_1745060.jpeg "week1新.jpg")

## **上周考试错题总结** ##

1.下列关于Java语言特点的描述，正确的一组是
C.面向对象；平台无关；动态
