import java.sql.*;

public class shm {
	public static void main(String args[]) {
		Connection con = null;
		Statement sql;
		ResultSet rs;
		float di = 50.0f;
		float gao = 50.0f;
		String country_low = "A";
		String country_high = "B";
		try {
			/* 加载JDBC_MySQL驱动 */
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
		}
		String uri = "jdbc:mysql://localhost:3306/world?useSSL=true";
		String user = "root";
		String password = "";
		try {
			con = DriverManager.getConnection(uri, user, password); //连接代码
		} catch (SQLException e) {
		}
		try {
			sql = con.createStatement();
			rs = sql.executeQuery("SELECT * FROM country"); //查询mess表
			while (rs.next()) {
				String name = rs.getString(2);
				float LifeExpectancy = rs.getFloat(8);
				if (LifeExpectancy < di && LifeExpectancy>0) {
					di = LifeExpectancy;
					country_low = name;
				}
				if (LifeExpectancy > gao) {
					gao = LifeExpectancy;
					country_high = name;
				}
			}
			con.close();
		}
		catch (SQLException e) {
			System.out.println(e);
		}

		System.out.printf("平均寿命最长的国家为： %s", country_high);
		System.out.printf("平均寿命最短的国家为： %s\n", country_low);
	}
}


